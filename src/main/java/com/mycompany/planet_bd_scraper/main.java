/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.planet_bd_scraper;

import com.mongodb.*;
import java.io.IOException;
//import java.util.regex.*;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        //connection database collection
        DBCollection collection = ConnectDB();
        
        Validate.isTrue(args.length == 1, "usage: supply url to fetch");
        String url = args[0];
        System.out.println("Fetching %s..."+ url);

        Document doc = Jsoup.connect(url).get();
        Elements links = doc.select("urlset url loc");
        
        System.out.println("\nLinks: (%d)"+ links.size());
        for (Element link : links) {
            System.out.println(link.text());
            BasicDBObject document = new BasicDBObject();
            document.put("url", link.text());
            
            String dl_link = BdLink(link.text());
            System.out.println("\t - "+dl_link);
            document.put("dl_link", dl_link);

            collection.insert(document);
            
        }

        
    }
    
    public static String BdLink(String url) throws IOException
    {
        //Pattern.matches("[0-9]+", url)
        Document doc = Jsoup.connect(url).get();
        Elements links = doc.select("a[href]");
        
        
        for (Element link : links) {
            String l = link.attr("href");
            if (l.contains("http://ul."))
                return l;
        }
        return "";
    }
    
    public static DBCollection ConnectDB()
    {
        MongoClient mongoClient = new MongoClient("localhost", 27017);
        DB database = mongoClient.getDB("planet-bd");
        database.createCollection("BD", null);
        
        DBCollection collection = database.getCollection("BD");
        
        return collection;
    }
    
}
